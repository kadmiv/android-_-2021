# Занятие 2 - Intents
нужно знать перед занятием:
- [Intens и intent фильтры](https://developer.android.com/guide/components/intents-filters?hl=ru)
- передача аргументов
- как получить переданный интент
- Serializable / [Parcelable](https://startandroid.ru/ru/uroki/vse-uroki-spiskom/132-urok-69-peredaem-parcelable-obekty-s-pomoschju-intent.html)

### домашка
Текстовое поле и несколько кнопок, поделиться с друзьями, совершить звонок, открыть в браузере
пишем свое приложение, которое тоже может делиться с друзьями
