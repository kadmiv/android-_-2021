# Занятие 0
- ОС Андроид 
- [dalvik и ART](https://source.android.com/devices/tech/dalvik/)
- компиляция (JIT/AOT)
- [dex](https://source.android.com/devices/tech/dalvik/dex-format)
- [C++, .os](https://developer.android.com/studio/projects/add-native-code)
- процессы, [и как оно там все работает](https://medium.com/@voodoomio/what-the-zygote-76f852d887d9)
- [Download](https://developer.android.com/studio/) this, and [Install](https://developer.android.com/studio/install) SDK
- Установка прав разработчика на телефоне - отладка по USB
- Установка и настройка эмуляторов Андроид


Андроид сложная, напичканая костылями, архитектура. Без знаний ее внутренних механизмов, Андроид накажет вас на первом же проекте.

![](https://camo.githubusercontent.com/d90e2ab5b8f61b7c8717da7e37a8be8c6838505a/68747470733a2f2f6c68332e676f6f676c6575736572636f6e74656e742e636f6d2f6f737752595875476342385959766b78474c43496942594f4a4951764e395a534d503255307336504757324c795363356736726c6c6f5a58586b6d7557776d4f6b30556a76513d77313336362d683736382d72772d6e6f)

[подробней о архитектуре ОС Андроид](http://www.c4learn.com/android/android-os-architecture/)


### Задание:
- [Download](https://developer.android.com/studio/) this, and [Install](https://developer.android.com/studio/install) SDK
 вывести на экран “Hello world”
