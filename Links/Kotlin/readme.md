# Java

- [гайд по Java](https://metanit.com/java/tutorial/)
- [гайд по Android](https://metanit.com/java/android/)


# Kotlin

- docs [ENG](https://kotlinlang.org/docs/reference/basic-syntax.html)/[RUS](https://kotlinlang.ru/)
- [31DaysOfKotlin ](https://medium.com/google-developers/31daysofkotlin-week-1-recap-fbd5a622ef86)
- [Hidden Gems In Kotlin StdLib](https://tech.okcupid.com/hidden-gems-in-kotlin-stdlib/)